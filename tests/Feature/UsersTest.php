<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    public function testUserGetIndexFails()
    {
        $userCount = 5;
        $user = factory(User::class, $userCount)->create()->first();

        $response = $this->actingAs($user)
            ->get('api/users');

        $response->assertStatus(405);
    }

    public function testUserGetOwnInfoSuccess()
    {
        $user = factory(User::class)->create()->first();

        $response = $this->actingAs($user)
            ->get("api/users/{$user->id}");

        $response->assertStatus(200)
            ->assertJson([
                'name'  => $user->name,
                'email' => $user->email,
            ])
            ->assertJsonMissing([
                'password',
            ]);
    }

    public function testUserGetOtherInfoFail()
    {
        $users = factory(User::class, 2)->create();

        $response = $this->actingAs($users[0])
            ->get("api/users/{$users[1]->id}");

        $response->assertStatus(403);
    }

    public function testUserGetNonexistingUserFail()
    {
        $user = factory(User::class)->create()->first();

        $response = $this->actingAs($user)
            ->get('api/users/100');

        $response->assertStatus(404);
    }

    public function testUserCreateSuccess()
    {
        $userData = [
            'name'     => 'Mike',
            'email'    => 'very_unique@example.com',
            'password' => 'password',
        ];

        $response = $this->post('api/users/', $userData);

        $response->assertStatus(201)
            ->assertJson([
                'name'  => $userData['name'],
                'email' => $userData['email'],
            ])
            ->assertJsonMissing([
                'password',
            ]);
    }

    public function testUserCreateNoRequestBody()
    {
        $response = $this->post('api/users/');

        $response->assertStatus(422)
            ->assertJsonValidationErrors([
                'name',
                'email',
                'password',
            ]);
    }

    /** @test */
    public function testUserCreateValuesTooShortBody()
    {
        $userData = [
            'name'     => 'A',
            'email'    => 'very_cool@example.com',
            'password' => 'pass',
        ];

        $response = $this->post('api/users/', $userData);

        $response->assertStatus(422)
            ->assertJsonValidationErrors([
                'name',
                'password',
            ]);
    }

    /** @test */
    public function testUserCreateInvalidEmail()
    {
        $userData = [
            'name'     => 'Mike',
            'email'    => 'not_valid@example',
            'password' => 'password',
        ];

        $response = $this->post('api/users/', $userData);

        $response->assertStatus(422)
            ->assertJsonValidationErrors([
                'email',
            ]);
    }

    public function testUserUpdateSuccess()
    {
        $user = factory(User::class)->create()->first();
        $userData = [
            'name'     => 'Joshua',
            'email'    => 'new_email@example.com',
            'password' => 'newpassword',
        ];

        $response = $this->actingAs($user)
            ->put("api/users/{$user->id}", $userData);

        $response->assertStatus(200)
            ->assertJson([
                'name'  => $userData['name'],
                'email' => $userData['email'],
            ]);

        $this->assertDatabaseHas('users', collect($userData)->except('password')->toArray());
    }

    public function testUserUpdateOtherUserFail()
    {
        $users = factory(User::class, 2)->create();
        $userData = [
            'name'     => 'Joshua',
            'email'    => 'new_email@example.com',
            'password' => 'newpassword',
        ];

        $response = $this->actingAs($users[0])
            ->put("api/users/{$users[1]->id}", $userData);

        $response->assertStatus(403);

        $this->assertDatabaseMissing('users', collect($userData)->except('password')->toArray());
    }

    public function testUserUpdatePartialSuccess()
    {
        $user = factory(User::class)->create()->first();
        $userData = [
            'name' => 'John', // Only change name
        ];
        $expected = [
            'id'    => $user->id,
            'name'  => $userData['name'],
            'email' => $user->email,
        ];

        $response = $this->actingAs($user)
            ->put("api/users/{$user->id}", $userData);

        $response->assertStatus(200)
            ->assertJson($expected);

        $this->assertDatabaseHas('users', $expected);
    }

    public function testUserUpdateWithValidationErrorsFail()
    {
        $user = factory(User::class)->create()->first();
        $userData = [
            'name'     => 'J', // Too Short
            'email'    => $user->email,
            'password' => 'newpass', // Too Short
        ];

        $response = $this->actingAs($user)
            ->put("api/users/{$user->id}", $userData);

        $response->assertStatus(422)
            ->assertJsonValidationErrors([
                'name', 'password',
            ]);
    }

    public function testUserDeleteSuccess()
    {
        $user = factory(User::class)->create()->first();

        $response = $this->actingAs($user)
            ->delete("api/users/{$user->id}");

        $response->assertStatus(200);
    }

    public function testUserDeleteOtherUserFail()
    {
        $users = factory(User::class, 2)->create();

        $response = $this->actingAs($users[0])
            ->delete("api/users/{$users[1]->id}");

        $response->assertStatus(403);
    }
}
