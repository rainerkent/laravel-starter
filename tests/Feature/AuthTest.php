<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use JWTAuth;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function testAuthLoginShouldBeSuccessful()
    {
        $user = factory(User::class)->create()->first();
        $requestData = [
            'email'    => $user->email,
            'password' => 'secret',
        ];

        $response = $this->post('api/auth/login', $requestData);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type',
                'expires_in',
            ]);
    }

    public function testAuthLoginWrongCredentialsFail()
    {
        $user = factory(User::class)->create()->first();
        $requestData = [
            'email'    => 'definitely_wrong@gmail.com',
            'password' => 'incorrect_password',
        ];

        $response = $this->post('api/auth/login', $requestData);

        $response->assertStatus(401)
            ->assertJson(['error' => 'Wrong credentials']);
    }

    public function testAuthGetTokenUserInformationSuccess()
    {
        $user = factory(User::class)->create()->first();
        $token = JWTAuth::fromUser($user);
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->withHeaders($headers)
            ->get('api/auth/me');

        $response->assertStatus(200)
            ->assertJson([
                'email' => $user->email,
                'name'  => $user->name,
            ]);
    }

    public function testAuthGetTokenUserInformationUnauthenticatedFail()
    {
        $response = $this->get('api/auth/me');

        $response->assertStatus(401);
    }

    public function testAuthLogoutSuccess()
    {
        $user = factory(User::class)->create()->first();
        $token = JWTAuth::fromUser($user);
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->withHeaders($headers)
            ->post('api/auth/logout');

        $response->assertStatus(200)
            ->assertJson(['message' => 'Successfully logged out']);
    }

    public function testAuthInvalidatedTokenShouldNotBeUsable()
    {
        $user = factory(User::class)->create()->first();
        $token = JWTAuth::fromUser($user);
        $headers = ['Authorization' => "Bearer $token"];

        // Logout using generated user token
        $this->withHeaders($headers)->post('api/auth/logout');

        // Try logging in using the token
        $response = $this->withHeaders($headers)->get('api/auth/me');

        $response->assertStatus(401);
    }

    public function testAuthForgotPasswordShouldSendEmail()
    {
        \Notification::fake();
        $user = factory(User::class)->create()->first();
        $requestData = ['email' => $user->email];

        // Send ResetPasswordNotification to user
        $response = $this->post('api/auth/password/email', $requestData);
        $response->assertStatus(200);

        \Notification::assertSentTo(
            $user,
            ResetPasswordNotification::class
        );
        $this->assertDatabaseHas('password_resets', ['email' => $user->email]);
    }

    public function testAuthForgotPasswordUsingNonRegisteredEmailShouldFail()
    {
        \Notification::fake();
        $requestData = ['email' => 'no_existing_email '];

        $response = $this->post('api/auth/password/email', $requestData);
        $response->assertStatus(422);

        \Notification::assertNothingSent();
        $this->assertDatabaseMissing('password_resets', []);
    }

    public function testAuthPasswordResetShouldResetUserPassword()
    {
        \Notification::fake();

        // Create user and send password reset token
        $user = factory(User::class)->create()->first();
        $this->post('api/auth/password/email', ['email' => $user->email]);

        // Get reset token from database
        $passwordReset = \Notification::sent($user, ResetPasswordNotification::class)->first();
        $newPassword = 'newP@ssw0rd';
        $requestBody = [
            'token'                 => $passwordReset->token,
            'email'                 => $user->email,
            'password'              => $newPassword,
            'password_confirmation' => $newPassword,
        ];

        // Reset user password
        $response = $this->post('api/auth/password/reset', $requestBody);
        $response->assertStatus(200);

        // Login using new credentials
        $response = $this->post('api/auth/login', [
            'email'    => $user->email,
            'password' => $newPassword,
        ]);
        $response->assertStatus(200);
    }

    public function testAuthPasswordResetUsingWrongEmailShouldFail()
    {
        \Notification::fake();

        // Create users and send password reset token
        $users = factory(User::class, 2)->create();
        $this->post('api/auth/password/email', ['email' => $users[0]->email]);

        // Get reset token from database
        $passwordReset = \Notification::sent($users[0], ResetPasswordNotification::class)->first();
        $newPassword = 'newP@ssw0rd';
        $requestBody = [
            'token'                 => $passwordReset->token,
            'email'                 => $users[1]->email, // used another user's email
            'password'              => $newPassword,
            'password_confirmation' => $newPassword,
        ];

        // Reset user password
        $response = $this->post('api/auth/password/reset', $requestBody);
        $response->assertStatus(422);
    }

    public function testAuthPasswordResetUsingNotMatchingPasswordShouldFail()
    {
        \Notification::fake();

        // Create users and send password reset token
        $user = factory(User::class)->create()->first();
        $this->post('api/auth/password/email', ['email' => $user->email]);

        // Get reset token from database
        $passwordReset = \Notification::sent($user, ResetPasswordNotification::class)->first();
        $newPassword = 'newP@ssw0rd';
        $requestBody = [
            'token'                 => $passwordReset->token,
            'email'                 => $user->email,
            'password'              => $newPassword,
            'password_confirmation' => 'secret', // used another password
        ];

        // Reset user password
        $response = $this->post('api/auth/password/reset', $requestBody);
        $response->assertStatus(422);
    }

    public function testAuthPasswordResetUsingInvalidTokenShouldFail()
    {
        \Notification::fake();

        // Create users and send password reset token
        $user = factory(User::class)->create()->first();
        $this->post('api/auth/password/email', ['email' => $user->email]);

        // Get reset token from database
        $passwordReset = \Notification::sent($user, ResetPasswordNotification::class)->first();
        $newPassword = 'newP@ssw0rd';
        $requestBody = [
            'token'                 => 'blahblahblah',
            'email'                 => $user->email,
            'password'              => $newPassword,
            'password_confirmation' => $newPassword,
        ];

        // Reset user password
        $response = $this->post('api/auth/password/reset', $requestBody);
        $response->assertStatus(422);
    }
}
